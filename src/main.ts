import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as passport from 'passport';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(passport.initialize());
  app.use(passport.session());
  app.setGlobalPrefix('mip/api/v1');
  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('MIP - Movimentação Interna de Produto')
    .setDescription('API dos serviços da MIP - Movimentação Interna de Produtos')
    .setVersion('1.0')
    .addTag('MIP')
    .setBasePath('mip/api/v1')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('mip/api/v1/swagger-ui', app, document);

  await app.listen(8233);
}
bootstrap();
