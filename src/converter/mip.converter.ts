import { JsonConverter, JsonCustomConvert } from "json2typescript";

@JsonConverter
export class DateConverter implements JsonCustomConvert<Date> {
    serialize(data: Date) {
        return data.toString();
    }    
    deserialize(data: any): Date {
        return new Date(data);
    }
}

@JsonConverter
export class BooleanConverter implements JsonCustomConvert<Boolean> {
    serialize(data: Boolean) {
        throw new Error("Method not implemented.");
    }    
    deserialize(data: any): Boolean {
        if(data){
            if(data === 1 || data === "1"){
                return true;
            }
        }
        return false;
    }

    
}