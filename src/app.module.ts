import { ControllerModule } from '@horus/node-rest';
import { SecurityModule } from '@horus/node-security';
import { Module } from '@nestjs/common';
import { EventoController } from './controller/evento.controller';
import { LocalEstoqueController } from './controller/local.estoque.controller';
import { MipController } from './controller/mip.controller';
import { MotivoMovimentacaoController } from './controller/motivo.movimentacao.controller';
import { WorkflowController } from './controller/workflow.controller';
import { DataBaseService } from './service/database.service';
import { EventoService } from './service/evento.service';
import { FilialService } from './service/filial.service';
import { LocalEstoqueService } from './service/local.estoque.service';
import { LoggerService } from './service/logger.service';
import { MipService } from './service/mip.serivce';
import { MotivoMovimentacaoService } from './service/motivo.movimentacao.service';
import { UsuarioService } from './service/usuario.service';
import { WorkflowMipPoolService } from './service/workflow.pool.service';


@Module({
  controllers: [
    EventoController,
    LocalEstoqueController,
    MipController,
    MotivoMovimentacaoController,
    WorkflowController
  ],
  providers: [
    {
        provide: LoggerService,
        useValue: new LoggerService()
    },
    DataBaseService,
    EventoService,
    FilialService,
    LocalEstoqueService,
    MipService,
    MotivoMovimentacaoService,
    UsuarioService,
    WorkflowMipPoolService
  ],
  imports: [
    SecurityModule.whiteList('actuator/health', 'swagger-ui'),
    ControllerModule
  ]
})
export class AppModule {}
