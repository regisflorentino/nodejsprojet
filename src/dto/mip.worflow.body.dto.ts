import { MipDTO } from "./mip.dto";

export class MipWorkflowBodyDTO {
    beanName: string;
    entity: MipDTO;
}