import { ApiModelProperty } from "@nestjs/swagger";
import { JsonProperty } from "json2typescript";
import { DateConverter, BooleanConverter } from "../converter/mip.converter";
import { UsuarioDTO } from "./usuario.dto";

export class HistoricoMipDTO {

    constructor() {
        this.id = undefined
        this.noDocumento = undefined
        this.type = undefined
        this.idUser = undefined
        this.descricao = undefined
        this.dataFato = undefined
    }

    
    @ApiModelProperty()
    @JsonProperty('ID', Number)
    id?: number;

    @ApiModelProperty()
    @JsonProperty('NO_DOCUMENTO', Number)
    noDocumento?: number;

    @ApiModelProperty()
    @JsonProperty('TYPE', String)
    type?: string;

    @ApiModelProperty()
    @JsonProperty('ID_USER', Number)
    idUser?: number;

    
    @ApiModelProperty()
    @JsonProperty('DESCRICAO', String)
    descricao?: string;

    @ApiModelProperty()
    @JsonProperty('DATA_FATO', DateConverter)
    dataFato?: Date;

    items?: HistoricoMipDTOItem[];
    usuario?: UsuarioDTO;

}


export class HistoricoMipDTOItem {

    constructor() {
        this.id = undefined
        this.idMipHistorico = undefined
        this.idEmbalagem = undefined
        this.embalagemMod = undefined
        this.idEmbalagemNovo = undefined
        this.quantidade = undefined
        this.quantidadeMod = undefined
        this.quantidadeNovo = undefined
        this.peso = undefined
        this.pesoMod = undefined
        this.pesoNovo = undefined
        this.custoMedio = undefined
        this.idProduto = undefined
        this.acao = undefined
        this.descricao = undefined
        this.dataFato = undefined
    }

    @ApiModelProperty()
    @JsonProperty('ID', Number)
    id?: number;

    @ApiModelProperty()
    @JsonProperty('ID_MIP_HISTORICO', Number)
    idMipHistorico?: number;

    @ApiModelProperty()
    @JsonProperty('ID_EMBALAGEM', Number, true)
    idEmbalagem?: number;

    @ApiModelProperty()
    @JsonProperty('EMBALAGEM_MOD', BooleanConverter, true)
    embalagemMod?: boolean;

    @ApiModelProperty()
    @JsonProperty('ID_EMBALAGEM_NOVO', Number, true)
    idEmbalagemNovo?: number;

    @ApiModelProperty()
    @JsonProperty('QUANTIDADE', Number, true)
    quantidade?: number;

    @ApiModelProperty()
    @JsonProperty('QUANTIDADE_MOD', BooleanConverter, true)
    quantidadeMod?: boolean;

    @ApiModelProperty()
    @JsonProperty('QUANTIDADE_NOVO', Number, true)
    quantidadeNovo?: number;

    @ApiModelProperty()
    @JsonProperty('PESO', Number, true)
    peso?: number;

    @ApiModelProperty()
    @JsonProperty('PESO_MOD', BooleanConverter, true)
    pesoMod?: boolean;

    @ApiModelProperty()
    @JsonProperty('PESO_NOVO', Number, true)
    pesoNovo?: number;

    @ApiModelProperty()
    @JsonProperty('CUSTO_MEDIO', Number, true)
    custoMedio?: number;

    @ApiModelProperty()
    @JsonProperty('ID_PRODUTO', Number)
    idProduto?: number;

    @ApiModelProperty()
    @JsonProperty('DESCRICAO', String)
    descricao?: string;

    @ApiModelProperty()
    @JsonProperty('ACTION', String, true)
    acao?: string;

    @ApiModelProperty()
    @JsonProperty('DATA_FATO', DateConverter)
    dataFato?: Date;

}


export class HistoricoResultMipDTO {
    id?: number;
    descricao?: string;
    dataFato?: string;
    items?: HistoricoResultItemDTO[];

}

export class HistoricoResultItemDTO {
    descricao?: string;
    acao?: string;
    alteracoes?: DataItemDTO[];
}

export class DataItemDTO {
    campo?: string;
    antigo?: number;
    novo?: number;
}