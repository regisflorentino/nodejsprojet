import { ApiModelProperty } from "@nestjs/swagger";
import { JsonProperty } from "json2typescript";
import { DateConverter } from "../converter/mip.converter";

export class UsuarioDTO {

    constructor(){

        this.id = undefined;
        this.idFilial = undefined;
        this.email = undefined;
        this.login = undefined;
        this.matricula = undefined;
        this.nome = undefined;
        this.status = undefined;
        this.terceiro = undefined;
        this.regional = undefined;
        this.ultimoAcesso = undefined;
        this.ultimaSenha = undefined;

    }

    @ApiModelProperty()
    @JsonProperty('ID_USUARIO', Number)
    id?: number;

    @ApiModelProperty()
    @JsonProperty('ID_FILIAL_BASE', Number)
    idFilial?: number;

    @ApiModelProperty()
    @JsonProperty('NM_EMAIL', String)
    email?: string;

    @ApiModelProperty()
    @JsonProperty('CD_LOGIN', String)
    login?: string;

    @ApiModelProperty()
    @JsonProperty('CD_MATRICULA', String)
    matricula?: string;

    @ApiModelProperty()
    @JsonProperty('NM_USUARIO', String)
    nome?: string;

    senha?: string;
   
    @ApiModelProperty()
    @JsonProperty('ST_USUARIO', Number, true)
    status?: boolean;
    
    @ApiModelProperty()
    @JsonProperty('FG_FUNC_TERCEIRO', Number, true)
    terceiro?: boolean;

    @ApiModelProperty()
    @JsonProperty('FG_REGIONAL', Number, true)
    regional?: boolean;
   
    @ApiModelProperty()
    @JsonProperty('DT_ULTIMO_ACESSO', DateConverter, true)
    ultimoAcesso?: Date;

    @ApiModelProperty()
    @JsonProperty('DT_ULT_SENHA', DateConverter, true)
    ultimaSenha?: Date;

    transacao?: number;
    refreshToken: number;
    perfis: any[];
    filiais: number[];
    
}