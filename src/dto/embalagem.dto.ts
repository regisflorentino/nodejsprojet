export class EmbalagemDTO {

    id?: number;
    produtoId?: number;
    pesoBruto?: number;
    pesoLiquido?: number;
    qtdUndEmbalagem?: number;
    tipoSigla?: string;
    tipoSiglaMenor?: string;
    pesavel?: boolean;
    ativo?: boolean;
    tipoQuantidadeEmbalagem?: 'MAIOR' | 'MENOR' | 'UNICA';
    selectLabel?: string;
}