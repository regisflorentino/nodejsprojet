import { ApiModelProperty } from "@nestjs/swagger";
import { JsonProperty } from "json2typescript";

export class FilialDTO {

    constructor(){
        this.id = undefined
        this.codigo = undefined
        this.nome = undefined
        this.idFornecedor = undefined
        this.idRegional = undefined
        this.prazoTransferenciaPadrao = undefined
        this.uf = undefined
        this.nomeExibicao = undefined
        this.tipo = undefined
        this.idAndNome = undefined
    }

    @ApiModelProperty()
    @JsonProperty('ID', Number)
    id?: number;

    @ApiModelProperty()
    @JsonProperty('CODIGO', Number)
    codigo?: number;

    @ApiModelProperty()
    @JsonProperty('NOME', String)
    nome?: string;

    @ApiModelProperty()
    @JsonProperty('IDFORNECEDOR', Number, true)
    idFornecedor?: number;

    @ApiModelProperty()
    @JsonProperty('IDREGIONAL', Number, true)
    idRegional?: number;

    @ApiModelProperty()
    @JsonProperty('PRAZOTRANSFERENCIAPADRAO', Number, true)
    prazoTransferenciaPadrao?: number;

    @ApiModelProperty()
    @JsonProperty('UF', String)
    uf?: string;

    @ApiModelProperty()
    @JsonProperty('NOMEEXIBICAO', String)
    nomeExibicao?: string;

    @ApiModelProperty()
    @JsonProperty('TIPO', Number)
    tipo?: number;

    @ApiModelProperty()
    @JsonProperty('IDANDNOME', String)
    idAndNome?: string;


}