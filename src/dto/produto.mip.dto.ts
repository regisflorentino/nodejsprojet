import { EmbalagemDTO } from "./embalagem.dto";
import { ProdutoDTO } from "./produto.dto";

export class ProdutoMipDTO {

    idMipItem?: number;
    produto?: ProdutoDTO;
    peso?: number;
    quantidade?: number;
    justificativa?: string;
    estoque?: number;
    embalagem?: EmbalagemDTO;
    custoMedio?: number;
    totalQtd?: number;
    totalPeso?: number;
    statusMovimentacao?: string;
    
}
