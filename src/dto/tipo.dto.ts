export interface TipoDTO {
    value?: string;
    label?: string;
}