import { ApiModelProperty } from '@nestjs/swagger';
import { JsonObject, JsonProperty } from "json2typescript";
@JsonObject("Evento")
export class EventoDTO {

    constructor() {
        this.idEventoEstoque = undefined;
        this.cdEventoEstoque = undefined;
        this.cdTipoEventoEstoque = undefined;
        this.dsEventoEstoque = undefined;
        this.idLocalEstoque = undefined;
        this.idAndName = undefined;
    }

    @ApiModelProperty()
    @JsonProperty('ID_EVENTO_ESTOQUE', Number)
    idEventoEstoque?: number;

    @ApiModelProperty()
    @JsonProperty('CD_EVENTO_ESTOQUE', String)
    cdEventoEstoque?: string;

    @ApiModelProperty()
    @JsonProperty('CD_TIPO_EVENTO_ESTOQUE', String)
    cdTipoEventoEstoque?: string;

    @ApiModelProperty()
    @JsonProperty('DS_EVENTO_ESTOQUE', String)
    dsEventoEstoque?: string;

    @ApiModelProperty()
    @JsonProperty('ID_LOCAL_ESTOQUE', Number, true)
    idLocalEstoque?: number;

    @ApiModelProperty()
    @JsonProperty('ID_AND_NAME', String)
    idAndName?: string;

}
