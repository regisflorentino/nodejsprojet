import { ApiModelProperty } from '@nestjs/swagger';
import { JsonProperty } from 'json2typescript';

export class LocalEstoqueDTO {

    constructor() {
        this.idLocalEstoque = undefined;
        this.descriptionLocalEstoque = undefined;
        this.idAndName = undefined;
    }

    @ApiModelProperty()
    @JsonProperty('ID_LOCAL_ESTOQUE', Number)
    idLocalEstoque?: number;

    @ApiModelProperty()
    @JsonProperty('NM_LOCAL_ESTOQUE', String)
    descriptionLocalEstoque?: string;

    @ApiModelProperty()
    @JsonProperty('ID_AND_NAME', String)
    idAndName?: string;

}
