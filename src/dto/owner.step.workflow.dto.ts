import { ApiModelProperty } from "@nestjs/swagger";
import { JsonProperty } from "json2typescript";
import { DateConverter } from "../converter/mip.converter";

export class OwnerStepWorkflowDTO {

    constructor(){
        this.step = undefined;
        this.dateAction = undefined;
        this.owner = undefined;
    }

    @ApiModelProperty()
    @JsonProperty('STEP_WKF', String)
    step: string;

    @ApiModelProperty()
    @JsonProperty('OWNER', String)
    owner: string;

    @ApiModelProperty()
    @JsonProperty('DATE_ACTION', DateConverter, true)
    dateAction: Date;

}

