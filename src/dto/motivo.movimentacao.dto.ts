import { ApiModelProperty } from "@nestjs/swagger";
import { JsonProperty } from "json2typescript";

export class MotivoMovimentacaoDTO {
    
    constructor() {
        this.idMotivoMovimentacao = undefined;
        this.descricaoMotivoMovimentacao = undefined;
        this.idEventoEstoque = undefined;
        this.idAndName = undefined;
    }

    @ApiModelProperty()
    @JsonProperty('ID_MOTIVO_MOVIMENTACAO', Number)
    idMotivoMovimentacao?: number;

    @ApiModelProperty()
    @JsonProperty('DS_MOTIVO_MOVIMENTACAO', String)
    descricaoMotivoMovimentacao?: string;
    
    @ApiModelProperty()
    @JsonProperty('ID_EVENTO_ESTOQUE', Number, true)
    idEventoEstoque?: number;

    @ApiModelProperty()
    @JsonProperty('ID_AND_NAME', String)
    idAndName?: string;

}