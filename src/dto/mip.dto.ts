import { ApiModelProperty } from "@nestjs/swagger";
import { JsonProperty } from "json2typescript";
import { DateConverter } from "../converter/mip.converter";
import { EventoDTO } from "./evento.dto";
import { FilialDTO } from "./filial.dto";
import { LocalEstoqueDTO } from "./local.estoque.dto";
import { MotivoMovimentacaoDTO } from "./motivo.movimentacao.dto";
import { ProdutoMipDTO } from "./produto.mip.dto";
import { TipoDTO } from "./tipo.dto";
import { UsuarioDTO } from "./usuario.dto";

export class MipDTO {

    id?: number;
    filial?: FilialDTO;
    numeroDocumento?: number;
    tipo?: TipoDTO;
    local?: LocalEstoqueDTO;
    origem?: LocalEstoqueDTO;
    destino?: LocalEstoqueDTO;
    evento?: EventoDTO;
    motivoMovimentacao?: MotivoMovimentacaoDTO;
    dataSolicitacao?: Date;
    status?: string;
    usuario?: UsuarioDTO;
    produtos?: ProdutoMipDTO[];
    nmSolicitante?: string;
    tpOrigem?: string;
    idDetalheWorkflow?: number;

}

export class MipCapaDTO{

    constructor(){
        this.idDocMip = undefined;
        this.filial = undefined;
        this.noDucumento = undefined;
        this.tipo = undefined;
        this.local = undefined;
        this.origem = undefined;
        this.destino = undefined;
        this.evento = undefined;
        this.status = undefined;
        this.nmSolicitacao = undefined;
        this.dtSolicitacao = undefined;
        this.movimentacao = undefined;
        this.tpOrigem = undefined;
    }

    @ApiModelProperty()
    @JsonProperty('ID_DOC_MIP', Number)
    idDocMip?: number;

    @ApiModelProperty()
    @JsonProperty('FILIAL', String)
    filial?: string;

    @ApiModelProperty()
    @JsonProperty('NO_DOCUMENTO', String)
    noDucumento?: string;

    @ApiModelProperty()
    @JsonProperty('TIPO', String, true)
    tipo?: string;

    @ApiModelProperty()
    @JsonProperty('LOCAL', String, true)
    local?: string;

    @ApiModelProperty()
    @JsonProperty('ORIGEM', String, true)
    origem?: string;

    @ApiModelProperty()
    @JsonProperty('DESTINO', String, true)
    destino?: string;

    @ApiModelProperty()
    @JsonProperty('EVENTO', String, true)
    evento?: string;

    @ApiModelProperty()
    @JsonProperty('STATUS', String)
    status?: string;

    @ApiModelProperty()
    @JsonProperty('NM_SOLICITACAO', String)
    nmSolicitacao?: string;

    @ApiModelProperty()
    @JsonProperty( 'DT_SOLICITACAO', DateConverter)
    dtSolicitacao?: Date;

    @ApiModelProperty()
    @JsonProperty('MOVIMENTACAO', String, true)
    movimentacao?: string;

    @ApiModelProperty()
    @JsonProperty('TP_ORIGEM', String)
    tpOrigem?: string;

}

export class MipItemsDTO {

    constructor(){
        this.idMipItem = undefined
        this.produtoId = undefined
        this.produtoCodigo = undefined
        this.produtoDescricao = undefined
        this.produtoAtivo = undefined
        this.produtoPesavel = undefined
        this.produtoLabel = undefined
        this.peso = undefined
        this.quantidade = undefined
        this.justificativa = undefined
        this.statusMovimentacao = undefined
        this.custoMedio = undefined;
        this.embalagemId = undefined
        this.embalagemProdutoId = undefined
        this.embalagemPesoBruto = undefined
        this.embalagemPesoLiquido = undefined
        this.embalagemQtdEmbalagem = undefined
        this.embalahemSigla = undefined
        this.embalagemPesavel = undefined
        this.embalagemAtivo = undefined
        this.embalagemTipoDescricao = undefined
        this.embalagemLabel = undefined
        this.embalagemMenorSigla = undefined
    }

    @ApiModelProperty()
    @JsonProperty('IDMIPITEM', Number)
    idMipItem?: number;

    @ApiModelProperty()
    @JsonProperty('IDPRODUTO', Number)
    produtoId: number;

    @ApiModelProperty()
    @JsonProperty('CODIGOPRODUTO', Number)
    produtoCodigo: number;

    @ApiModelProperty()
    @JsonProperty('DESCRICAO', String)
    produtoDescricao: string;

    @ApiModelProperty()
    @JsonProperty('PRODUTOATIVO', Number, true)
    produtoAtivo: boolean;

    @ApiModelProperty()
    @JsonProperty('PESAVEL', String)
    produtoPesavel: string

    @ApiModelProperty()
    @JsonProperty('PRODUTOLABEL', String)
    produtoLabel: string;

    @ApiModelProperty()
    @JsonProperty('PESO', Number, true)
    peso: number;

    @ApiModelProperty()
    @JsonProperty('QTD', Number, true)
    quantidade: number;

    @ApiModelProperty()
    @JsonProperty('CUSTOMEDIOPRODUTO', Number, true)
    custoMedio: number;

    @ApiModelProperty()
    @JsonProperty('ST_MOVIMENTACAO', String, true)
    statusMovimentacao: string;

    @ApiModelProperty()
    @JsonProperty('JUSTIFICATIVA', String, true)
    justificativa: string;

    @ApiModelProperty()
    @JsonProperty('IDEMBALAGEM', Number)
    embalagemId: number;

    @ApiModelProperty()
    @JsonProperty('EMBIDPRODUTO', Number)
    embalagemProdutoId: number;

    @ApiModelProperty()
    @JsonProperty('PESOBRUTO', Number, true)
    embalagemPesoBruto: number;

    @ApiModelProperty()
    @JsonProperty('PESOLIQUIDO', Number, true)
    embalagemPesoLiquido: number;

    @ApiModelProperty()
    @JsonProperty('QTDEMBALGEM', Number, true)
    embalagemQtdEmbalagem: number;

    @ApiModelProperty()
    @JsonProperty('EMBTIPOSIGLA', String)
    embalahemSigla: string;

    @ApiModelProperty()
    @JsonProperty('EMBPESAVEL', Number, true)
    embalagemPesavel: boolean;

    @ApiModelProperty()
    @JsonProperty('EMBATIVO', Number)
    embalagemAtivo: boolean;

    @ApiModelProperty()
    @JsonProperty('EMBTIPODESCRICAO', String, true)
    embalagemTipoDescricao: string;

    @ApiModelProperty()
    @JsonProperty('EMBALAGEMLABEL', String)
    embalagemLabel: string;

    @ApiModelProperty()
    @JsonProperty('MENORSIGLA', String)
    embalagemMenorSigla: string;

}