import { MipDTO } from "./mip.dto";

export class EstornoMipDTO {
    mipAntesEstorno?: MipDTO;
    mipAtualizada?: MipDTO;
}