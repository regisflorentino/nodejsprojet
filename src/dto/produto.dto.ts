export class ProdutoDTO {
    id: number;
    codigo?: number;
    descricao?: string;
    ativo?: boolean;
    pesavel?: string;
    idAndNome?: string;
}