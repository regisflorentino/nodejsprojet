import { ApiModelProperty } from "@nestjs/swagger";
import { JsonProperty } from "json2typescript";
import { DateConverter } from "../converter/mip.converter";

export class EstornoItemDTO {

    constructor() {
        this.id = undefined;
        this.pesoAnterior = undefined;
        this.pesoNovoValor = undefined;
        this.quantidadeAnterior = undefined;
        this.quantidadeNovoValor = undefined;
        this.embalagemAnterior = undefined;
        this.embalagemNovoValor = undefined;
        this.idProduto = undefined;
        this.idFilial = undefined;
        this.idDocMip = undefined;
        this.idDocMipItem = undefined;
        this.dtFato = undefined;
    }

    @ApiModelProperty()
    @JsonProperty('ID', Number)
    id?: number;

    @ApiModelProperty()
    @JsonProperty('PESO_ANTERIOR', Number, true)
    pesoAnterior?: number;

    @ApiModelProperty()
    @JsonProperty('PESO_NOVO_VALOR', Number, true)
    pesoNovoValor?: number;

    @ApiModelProperty()
    @JsonProperty('QTD_ANTERIOR', Number, true)
    quantidadeAnterior?: number;

    @ApiModelProperty()
    @JsonProperty('QTD_NOVO_VALOR', Number, true)
    quantidadeNovoValor?: number;

    @ApiModelProperty()
    @JsonProperty('EMBALAGEM_ANTERIOR', Number, true)
    embalagemAnterior?: number;

    @ApiModelProperty()
    @JsonProperty('EMBALAGEM_NOVO_VALOR', Number, true)
    embalagemNovoValor?: number;

    @ApiModelProperty()
    @JsonProperty('ID_PRODUTO', Number)
    idProduto?: number;

    @ApiModelProperty()
    @JsonProperty('ID_FILIAL', Number)
    idFilial?: number;

    @ApiModelProperty()
    @JsonProperty('ID_DOC_MIP', Number)
    idDocMip?: number;

    @ApiModelProperty()
    @JsonProperty('ID_DOC_MIP_ITEM', Number)
    idDocMipItem?: number;

    @ApiModelProperty()
    @JsonProperty('DT_FATO', DateConverter)
    dtFato?: Date;
}