import { Injectable } from "@nestjs/common";
import { MipService } from "./mip.serivce";

import { WorkflowPoolService } from '@horus/node-business';

@Injectable()
export class WorkflowMipPoolService implements WorkflowPoolService {
    
    constructor(private mipService: MipService){

    }

    async processWorkflow(parameter: any): Promise<number>{
        switch(parameter.beanName){
            case 'MIP_WORKFLOW_BEAN' :
               return await this.mipService.mergeWorkflow(parameter.entity);
            default:
                return null;
        }
    }

}