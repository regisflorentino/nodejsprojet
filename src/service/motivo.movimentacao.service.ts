import { Injectable } from "@nestjs/common";
import { DataBaseService } from "./database.service";
import { findAllByEvento, findById } from "../repository/motivo.movimentacao.queries";
import { MotivoMovimentacaoDTO } from "../dto/motivo.movimentacao.dto";
import { JsonConvert } from "json2typescript";

@Injectable()
export class MotivoMovimentacaoService {

    jsonConvert: JsonConvert;

    constructor(private dbService: DataBaseService){
        this.jsonConvert = new JsonConvert();
    }

    async findMotivoMovimentacaoByEvento(idEvento: number): Promise<MotivoMovimentacaoDTO[]>{
        
        let binds: any = {};
        binds.idEvento = idEvento;

        const result = await this.dbService.executeQueryWithObjectBind(findAllByEvento, binds);
        return this.jsonConvert.deserializeArray(result['rows'], MotivoMovimentacaoDTO)
    }

    async findById(idMotivo: number): Promise<MotivoMovimentacaoDTO>{

        if(!idMotivo){
            return null;
        }

        let binds: any = {};
        binds.idMotivo = idMotivo;

        let result = await this.dbService.executeQueryWithObjectBind(findById, binds);
        return this.jsonConvert.deserializeObject(result['rows'][0], MotivoMovimentacaoDTO);

    }

}