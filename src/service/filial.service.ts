import { Injectable } from "@nestjs/common";
import { DataBaseService } from "./database.service";
import { FilialDTO } from "../dto/filial.dto";
import { findFilialById } from "../repository/filial.queries";
import { JsonConvert } from "json2typescript";

@Injectable()
export class FilialService {

    jsonConvert: JsonConvert;

    constructor(private dbService: DataBaseService){
        this.jsonConvert = new JsonConvert();
    }

    async findById(idFilial: number): Promise<FilialDTO>{
        
        if(!idFilial){
            return null;
        }

        let binds: any = {};
        binds.idFilial = idFilial;

        const result = await this.dbService.executeQueryWithObjectBind(findFilialById, binds);
        return this.jsonConvert.deserializeObject(result['rows'][0], FilialDTO);
    }

}