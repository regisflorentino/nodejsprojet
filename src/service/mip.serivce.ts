import { Injectable } from "@nestjs/common";
import * as oracledb from 'oracledb';
import { MipCapaDTO, MipDTO, MipItemsDTO } from "../dto/mip.dto";
import { TipoDTO } from "../dto/tipo.dto";
import { findAllMipPaginated, findAllWorkflowMipByStatus, findCapaIdFromDetalheId, findMipById, findMipDocId, findMipItemsByCapaId, findMipNumberDocument, findOwnersStepWorkflow, insertMipCapa, insertMipDetalhe, insertEstornoMipHorus, updateMipDetalhe, updateStatusMipCapa, deleteMipItem, insertHistoricoMip, findHistoricoId, insertHistoricoMipItem, findHistoricoMipByNumeroDocumento, findItemsHistoricoByhistoricoId } from "../repository/mip.queries";
import { DataBaseService } from "./database.service";
import { EventoService } from "./evento.service";
import { FilialService } from "./filial.service";
import { LocalEstoqueService } from "./local.estoque.service";
import { MotivoMovimentacaoService } from "./motivo.movimentacao.service";
import { OwnerStepWorkflowDTO } from "../dto/owner.step.workflow.dto";
import { UsuarioService } from "./usuario.service";
import { EstornoMipDTO } from "../dto/estorno.mip.dto";
import { EstornoItemDTO } from "../dto/estorno.item.dto";
import { ProdutoMipDTO } from "../dto/produto.mip.dto";
import { HistoricoMipDTO, HistoricoResultMipDTO, HistoricoMipDTOItem, DataItemDTO, HistoricoResultItemDTO } from "../dto/historico.mip.dto";
import { JsonConvert } from "json2typescript";
import * as moment from 'moment';
@Injectable()
export class MipService {
    
    jsonConvert: JsonConvert;

    constructor(
        private dbService: DataBaseService, 
        private filialService: FilialService,
        private localService: LocalEstoqueService,
        private eventoService: EventoService,
        private motivoMovimentacao: MotivoMovimentacaoService,
        private usuarioService: UsuarioService){
            this.jsonConvert = new JsonConvert();
        }

    async findNextNextDocument(): Promise<Number>{
        let result = await this.dbService.executeQueryWithObjectBind(findMipNumberDocument);
        return result['rows'][0];
    }

    async findAllWorkflowMipByStatus(status: string, itemsPerPage: number, page: number){

        let binds: any = {};
        binds.status = status;
        binds.itemsPerPage = itemsPerPage;
        binds.page = page;

        let resultList: MipDTO[] = [];
        let result = await this.dbService.executeQueryWithObjectBind(findAllWorkflowMipByStatus, binds, { fetchInfo: {"DADOS_JSON": {type: oracledb.STRING } }});
        result['rows'].map(item => {
            const mipItem: MipDTO = JSON.parse(item['DADOS_JSON']);
            mipItem.id = item['ID'];
            resultList.push(mipItem);
        });
        return resultList;

    }

    async save(mipObject: MipDTO): Promise<number>{
        
        console.log('Salvando Mip');

        let result = await this.dbService.executeQueryWithObjectBind(findMipDocId);
        const idMip: number = result['rows'][0]['NEXTVAL']
        
        let bindsInsertMip: any = {};
        bindsInsertMip.idMip = idMip;
        bindsInsertMip.idFilial = mipObject.filial.id;
        bindsInsertMip.numeroDocumento = mipObject.numeroDocumento;
        bindsInsertMip.tipo =  mipObject.tipo.value;
        bindsInsertMip.evento = mipObject.evento ? mipObject.evento.idEventoEstoque : null;
        bindsInsertMip.local = mipObject.local ? mipObject.local.idLocalEstoque : null;
        bindsInsertMip.origem = mipObject.origem ? mipObject.origem.idLocalEstoque : null;
        bindsInsertMip.destino = mipObject.destino ? mipObject.destino.idLocalEstoque : null;
        bindsInsertMip.status = mipObject.status;
        bindsInsertMip.idUsuarioCadastro = mipObject.usuario.id;
        bindsInsertMip.idUsuarioAlteracao = null
        bindsInsertMip.dtAlteracao = null;
        bindsInsertMip.idDetalheWkf = mipObject.id;
        bindsInsertMip.nmSolicitante = mipObject.nmSolicitante;
        bindsInsertMip.dataSolicitacao = new Date(mipObject.dataSolicitacao);
        bindsInsertMip.motivoMovimentacao = mipObject.motivoMovimentacao? mipObject.motivoMovimentacao.idMotivoMovimentacao : null;
        bindsInsertMip.tpOrigem = mipObject.tpOrigem;
                
        await this.dbService.executeQueryWithObjectBind(insertMipCapa, bindsInsertMip);
        
        let bindsInsertDetalheList: any[] = [];
        mipObject.produtos.forEach(p => {
            let bindsInsertDetalhe: any = {};
            bindsInsertDetalhe.idMip = idMip;
            bindsInsertDetalhe.idProduto = p.produto.id;
            bindsInsertDetalhe.idEmbalagem = p.embalagem.id;
            bindsInsertDetalhe.qtdProduto = p.quantidade ? p.quantidade : null;
            bindsInsertDetalhe.pesoProduto = p.peso ? p.peso : null;
            bindsInsertDetalhe.justificativa = p.justificativa;
            bindsInsertDetalhe.statusMovimentacao = p.statusMovimentacao;
            bindsInsertDetalhe.custoMedioProduto = p.custoMedio;
            bindsInsertDetalheList.push(bindsInsertDetalhe);
        });
        
        if(bindsInsertDetalheList && bindsInsertDetalheList.length > 0){
            await this.dbService.executeQueryWithListBind(insertMipDetalhe, bindsInsertDetalheList);
        }

        console.log('Mip Salva. Id: ' + idMip);
        return idMip;
    }

    async findAllMip(itemsPerPage: number, page: number){

        let bindsFilter: any = {};
        bindsFilter.itemsPerPage = itemsPerPage;
        bindsFilter.page = page;

        let result = await this.dbService.executeQueryWithObjectBind(findAllMipPaginated, bindsFilter);
        return this.jsonConvert.deserializeArray(result['rows'], MipCapaDTO);
    }

    async findMipById(idMip: number) {

        let bind: any = {};
        bind.idMip = idMip;

        let result = await this.dbService.executeQueryWithObjectBind(findMipById, bind);
        let data = result['rows'][0];

        let mip: MipDTO = {};
        mip.id = data.ID_DOC_MIP;
        mip.idDetalheWorkflow = data.ID_DETALHE_WKF;

        mip.filial = await this.filialService.findById(data.ID_FILIAL);
        mip.origem = await this.localService.findById(data.ID_LOCAL_ESTOQUE_ORIGEM);
        mip.destino = await this.localService.findById(data.ID_LOCAL_ESTOQUE_DESTINO);
        mip.local = await this.localService.findById(data.ID_LOCAL_ESTOQUE);
        mip.usuario = await this.usuarioService.findById(data.ID_USUARIO_CADASTRO);
        
        mip.evento = await this.eventoService.findById(data.ID_EVENTO_ESTOQUE);
        if(mip.evento){
            mip.evento.idLocalEstoque = mip.local ? mip.local.idLocalEstoque : null;
        }
        
        mip.motivoMovimentacao = await this.motivoMovimentacao.findById(data.ID_MOTIVO_MOVIMENTACAO);
        if(mip.motivoMovimentacao){
            mip.motivoMovimentacao.idEventoEstoque = mip.evento ? mip.evento.idEventoEstoque : null;
        }

        mip.nmSolicitante = data.NM_USUARIO_SOLICITACAO;
        mip.numeroDocumento = data.NO_DOCUMENTO;
        mip.status = data.STATUS;
        mip.dataSolicitacao = data.DT_SOLICITACAO;
        mip.tpOrigem = data.TP_ORIGEM;
        mip.tipo = this.getTipo(data.TP_DOCUMENTO);

        return mip;

    }

    async findItemsDetalheMipByCapaId(idCapa: number){

        let bind: any = {};
        bind.idCapa = idCapa;

        let result = await this.dbService.executeQueryWithObjectBind(findMipItemsByCapaId, bind);
        return this.jsonConvert.deserializeArray(result['rows'], MipItemsDTO);

    }

    async findOwnerStepWKF(idDetalhe: number){

        let bind: any = {};
        bind.idDetalhe = idDetalhe;

        let resultCapa = await this.dbService.executeQueryWithObjectBind(findCapaIdFromDetalheId, bind);
        const idCapa = resultCapa['rows'][0]['ID_WKF_GENERICO_CAPA'];

        let bindDetalhe: any = {}
        bindDetalhe.idCapa = idCapa;

        let resultOwners = await this.dbService.executeQueryWithObjectBind(findOwnersStepWorkflow, bindDetalhe);
        return this.jsonConvert.deserializeArray(resultOwners['rows'], OwnerStepWorkflowDTO);
    }

    async findHistoricoByNumeroDocumento(noDocumento: number): Promise<HistoricoMipDTO[]> {
        let historicoFromDb = await this.dbService.executeQueryWithObjectBind(findHistoricoMipByNumeroDocumento, {noDocumento: noDocumento});
        const historicoListDTO: HistoricoMipDTO[] = this.jsonConvert.deserializeArray(historicoFromDb['rows'], HistoricoMipDTO);

        const historico: any[] = [];

        historicoListDTO.forEach(async h => {
            
            const historicoPai: HistoricoResultMipDTO = {};
            historicoPai.id = h.id;
            historicoPai.dataFato = moment(h.dataFato).format('DD/MM/YYYY HH:mm:ss');
            historicoPai.descricao = h.descricao;

            historico.push(
                this.dbService.executeQueryWithObjectBind(findItemsHistoricoByhistoricoId, {idHistorico: h.id}).then((historicoFilhoFromDb) => {
                    if(historicoFilhoFromDb && historicoFilhoFromDb['rows'] && historicoFilhoFromDb['rows'].length > 0){
                        const historicoFilhoListDTO: HistoricoMipDTOItem[] = this.jsonConvert.deserializeArray(historicoFilhoFromDb['rows'], HistoricoMipDTOItem);
                        
                        const historicoItems: HistoricoResultItemDTO[] = [];
                        historicoFilhoListDTO.forEach(i => {
                            const item: HistoricoResultItemDTO = {}
                            item.acao = i.acao;
                            item.descricao = i.descricao;
    
                            const itemsDTO: DataItemDTO[] = [];
                            if(i.embalagemMod){
                                itemsDTO.push(
                                    {
                                        antigo: i.idEmbalagem, 
                                        novo: i.idEmbalagemNovo, 
                                        campo: 'Embalagem'
                                    }
                                );
                            }
                            if(i.quantidadeMod){
                                itemsDTO.push(
                                    {
                                        antigo: i.quantidade, 
                                        novo: i.quantidadeNovo, 
                                        campo: 'Quantidade'
                                    }
                                );
                            }
                            if(i.pesoMod){
                                itemsDTO.push(
                                    {
                                        antigo: i.peso, 
                                        novo: i.pesoNovo, 
                                        campo: 'Peso'
                                    }
                                );
                            }
                            item.alteracoes = itemsDTO && itemsDTO.length > 0 ? itemsDTO : null;
                            historicoItems.push(item);
                        });
                        historicoPai.items = historicoItems;
                        return historicoPai;
                    } else {
                        return historicoPai;
                    }
                })
            );
        });
        return Promise.all(historico)
    }

    async mergeWorkflow(mipEntity: MipDTO): Promise<number>{
        return await this.save(mipEntity);
    }

    async saveHistorico(historicoDTO: HistoricoMipDTO){
        
        let resultId = await this.dbService.executeQueryWithObjectBind(findHistoricoId);
        const idMipHistorico: number = resultId['rows'][0]['NEXTVAL']

        historicoDTO.id = idMipHistorico;
        const bindInsertCapa: any = {};
        bindInsertCapa.id = historicoDTO.id,
        bindInsertCapa.noDocumento = historicoDTO.noDocumento,
        bindInsertCapa.type = historicoDTO.type,
        bindInsertCapa.idUser = historicoDTO.idUser,
        bindInsertCapa.descricao = historicoDTO.descricao,
        await this.dbService.executeQueryWithObjectBind(insertHistoricoMip, bindInsertCapa);

        if(historicoDTO.items && historicoDTO.items.length > 0){
            
            const bindsItemsHistorico: any[] = [];
            historicoDTO.items.forEach(i => {
                
                const bindItem: any = {};
                bindItem.idMipHistorico = idMipHistorico;
                bindItem.idEmbalagem = i.idEmbalagem;
                bindItem.embalagemMod = i.embalagemMod ? 1 : 0;
                bindItem.idEmbalagemNovo = i.idEmbalagemNovo;
                bindItem.quantidade = i.quantidade;
                bindItem.quantidadeMod = i.quantidadeMod ? 1 : 0;
                bindItem.quantidadeNovo = i.quantidadeNovo;
                bindItem.peso = i.peso;
                bindItem.pesoMod = i.pesoMod ? 1 : 0;
                bindItem.pesoNovo = i.pesoNovo;
                bindItem.custoMedio = i.custoMedio ? i.custoMedio : 0;
                bindItem.idProduto = i.idProduto;
                bindItem.descricao = i.descricao;
                bindItem.acao = i.descricao.indexOf('Exclusão') >= 0 ? 'remove' : i.descricao.indexOf('Edição')>= 0 ? 'edit' : 'new'

                bindsItemsHistorico.push(bindItem);
            });
            await this.dbService.executeQueryWithListBind(insertHistoricoMipItem, bindsItemsHistorico);
        }
    }

    async estornarMip(estornoDTO: EstornoMipDTO, tipo: string){
        if(tipo === 'TOTAL'){
            await this.estornarTotal(estornoDTO.mipAtualizada);
        } else {
            await this.estornarParcial(estornoDTO)
        }
    }

    async estornarTotal(mip2Estornar: MipDTO){

        const valuesEstorno: EstornoItemDTO[] = [];
        const bindsUpdate: any[] = [];

        const estornoTipo: string = 'ESTORNO MIP TOTAL'

        mip2Estornar.produtos.forEach(p => {
            const itemEstorno: EstornoItemDTO = this.buildItemsEstorno(p, null, mip2Estornar.id, mip2Estornar.filial.id, 'TOTAL');
            valuesEstorno.push(itemEstorno);
            bindsUpdate.push(this.buildBindsUpdate(itemEstorno, p, estornoTipo));
        });
        await this.updateMipDetalhe(bindsUpdate);
        await this.insertEstornoAndUpdateStatusCapa(valuesEstorno,{status: estornoTipo, idDocMip: mip2Estornar.id})
    }

    async estornarParcial(estornoDTO: EstornoMipDTO){

        let estornoTipo: string = 'ESTORNO PARCIAL POR PRODUTO';

        const valuesEstorno: EstornoItemDTO[] = [];
        const bindsUpdate: any[] = [];

        const idDocMip: number = estornoDTO.mipAntesEstorno.id;
        const idFilial: number = estornoDTO.mipAntesEstorno.filial.id;
        
        estornoDTO.mipAntesEstorno.produtos.forEach(async p => {
            
            const produtoEstornado: ProdutoMipDTO = estornoDTO.mipAtualizada.produtos.find(i => i.idMipItem === p.idMipItem);

            if(!produtoEstornado){
                valuesEstorno.push(this.buildItemsEstorno(p, null, idDocMip, idFilial, 'TOTAL'));
                await this.dbService.executeQueryWithObjectBind(deleteMipItem, {idDocMipItem: p.idMipItem});
                estornoTipo = 'ESTORNO TOTAL POR PRODUTO'
                return;
            }

            if( produtoEstornado.quantidade === p.quantidade && produtoEstornado.peso === p.peso && produtoEstornado.embalagem.id === p.embalagem.id){
                return;
            }

            const itemEstorno: EstornoItemDTO = this.buildItemsEstorno(p, produtoEstornado, idDocMip, idFilial, 'PARCIAL');
            if(p.produto.pesavel === 'PESÁVEL'){
                estornoTipo = p.peso - produtoEstornado.peso === 0 ? 'ESTORNO TOTAL POR PRODUTO' : estornoTipo;
            } else {
                estornoTipo = p.quantidade - produtoEstornado.quantidade === 0 ? 'ESTORNO TOTAL POR PRODUTO' : estornoTipo;
            }
            valuesEstorno.push(itemEstorno);
            bindsUpdate.push(this.buildBindsUpdate(itemEstorno, produtoEstornado, estornoTipo));
        });
        
        if(bindsUpdate.length > 0){
            await this.updateMipDetalhe(bindsUpdate);
        }
        if(valuesEstorno.length > 0){
            await this.insertEstornoAndUpdateStatusCapa(valuesEstorno, {status: estornoTipo, idDocMip: estornoDTO.mipAntesEstorno.id})
        }

    }

    async updateMipDetalhe(binds: any[]){
        await this.dbService.executeQueryWithListBind(updateMipDetalhe, binds);
    }

    async insertEstornoAndUpdateStatusCapa(bindsEstorno: any[], bindStatus: any){
        await this.dbService.executeQueryWithListBind(insertEstornoMipHorus, bindsEstorno);
        await this.dbService.executeQueryWithObjectBind(updateStatusMipCapa, bindStatus);
    }

    private buildItemsEstorno(produtoMipOld: ProdutoMipDTO, produtoMipNew: ProdutoMipDTO, idDocMip: number, idFilial: number, tipo: string): EstornoItemDTO{
        const itemEstorno: EstornoItemDTO = {};
        
        if(produtoMipOld.produto.pesavel === 'PESÁVEL'){
            itemEstorno.pesoAnterior = produtoMipOld.peso;
            itemEstorno.pesoNovoValor = tipo === 'TOTAL' ? 0 : produtoMipNew.peso;
            itemEstorno.quantidadeAnterior = null;
            itemEstorno.quantidadeNovoValor = null;
        } else {
            itemEstorno.quantidadeAnterior = produtoMipOld.quantidade;
            itemEstorno.quantidadeNovoValor = tipo === 'TOTAL' ? 0 : produtoMipNew.quantidade;;
            itemEstorno.pesoAnterior = null;
            itemEstorno.pesoNovoValor = null;
        }

        itemEstorno.embalagemAnterior = produtoMipOld.embalagem.id;
        if(produtoMipNew && produtoMipOld.embalagem.id !== produtoMipNew.embalagem.id && tipo === 'PARCIAL'){
            itemEstorno.embalagemNovoValor = produtoMipNew.embalagem.id;
        }else {
            itemEstorno.embalagemNovoValor = null;
        }
        
        itemEstorno.idDocMip = idDocMip;
        itemEstorno.idFilial = idFilial;
        itemEstorno.idDocMipItem = produtoMipOld.idMipItem;
        itemEstorno.idProduto = produtoMipOld.produto.id;
        
        return itemEstorno;
    }

    private buildBindsUpdate(itemEstorno: EstornoItemDTO, produtoMip: ProdutoMipDTO, tipo: string){
        const bindUpdate: any = {}
        bindUpdate.qtdProduto = itemEstorno.quantidadeNovoValor;
        bindUpdate.pesoProduto = itemEstorno.pesoNovoValor;
        bindUpdate.justificativa = produtoMip.justificativa ? produtoMip.justificativa : null;
        bindUpdate.statusMovimentacao = tipo;
        bindUpdate.custoMedioProduto = tipo.indexOf('TOTAL') < 0 ? produtoMip.custoMedio : 0;
        bindUpdate.idEmbalagem = produtoMip.embalagem.id;
        bindUpdate.idDocMipItem = produtoMip.idMipItem;
        return bindUpdate;
    }

    private getTipo(tipo: string): TipoDTO{
        let tipoModel: TipoDTO = {};
        tipoModel.value = tipo;
        if(tipo === 'MOVIMENTACAO'){
            tipoModel.label = 'MOVIMENTAÇÃO';
        }else {
            tipoModel.label = 'TRANSFERÊNCIA';
        }
        return tipoModel;
    }

}