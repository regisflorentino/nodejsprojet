import { Injectable } from "@nestjs/common";
import { DataBaseService } from "./database.service";
import { UsuarioDTO } from "../dto/usuario.dto";
import { findById } from "../repository/usuario.queries";
import { JsonConvert } from "json2typescript";

@Injectable()
export class UsuarioService {

    jsonConvert: JsonConvert;

    constructor(private dbService: DataBaseService){
        this.jsonConvert = new JsonConvert();
    }

    async findById(idUsuario: number): Promise<UsuarioDTO>{

        if(!idUsuario){
            return null;
        }

        let binds: any = {};
        binds.idUsuario = idUsuario;

        let result = await this.dbService.executeQueryWithObjectBind(findById, binds);
        return this.jsonConvert.deserializeObject(result['rows'][0], UsuarioDTO);

    }

}