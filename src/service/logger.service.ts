import { Injectable } from '@nestjs/common';
import { configure, getLogger, Configuration, shutdown } from 'log4js';
@Injectable()
export class LoggerService {

    logger: any;
    private grayLogger: any;

    constructor() {
        configure({
            appenders: { 
                access: {
                    type: "dateFile",
                    filename: "log/access.log",
                    pattern: "-yyyy-MM-dd",
                    category: "http"
                },
                app: {
                    type: "file",
                    filename: "log/app.log",
                    maxLogSize: 10485760,
                    numBackups: 3
                  },
                errorFile: {
                    type: "file",
                    filename: "log/errors.log"
                },
                errors: {
                    type: "logLevelFilter",
                    level: "ERROR",
                    appender: "errorFile"
                }
            },    
            categories: {
                default: { 
                    appenders: [ "app", "errors" ], 
                    level: "DEBUG" 
                },
                http: { 
                    appenders: [ "access"], 
                    level: "DEBUG" 
                }
            }
        });
        this.logger = getLogger();
        this.logger.level = 'debug';


        this.grayLogger = require('gelf-pro');
        this.grayLogger.setConfig({
            fields: {Application: "MIP", facility: "MIP"},
            adapterName: 'tcp',
            adapterOptions: {
              host: 'graylog-aws.corp.assai.com.br',
              port: 12205, 
              family: 4,
              timeout: 1000
            }
          });

    }

    debug(log: any) {
        this.logger.debug(log);
        this.grayLogger.debug(log);
    }

    info(log: any) {
        this.logger.info(log);
        this.grayLogger.info(log);
    }

    error(log: any) {
        this.logger.error(log);
        this.grayLogger.error(log);
    }

}
