import { Injectable } from "@nestjs/common";
import { DataBaseService } from "./database.service";
import { LoggerService } from "./logger.service";
import { EventoDTO } from "../dto/evento.dto";
import { findAllByUsuarioAndLocalAndEstoque, findById } from "../repository/evento.queries";
import { JsonConvert } from "json2typescript";

@Injectable()
export class EventoService {

    jsonConvert: JsonConvert;

    constructor(private dbService: DataBaseService, private loggerService: LoggerService){
        this.jsonConvert = new JsonConvert();
    }

    async findEventoByUserAndFilialAndLocalEstoque(idUser: number, idFilial: number, idLocalEstoque: number): Promise<EventoDTO[]> {
        
        let binds: any = {};
        binds.idUsuario = idUser;
        binds.idFilial = idFilial;
        binds.idLocalEstoque = idLocalEstoque;

        const result = await this.dbService.executeQueryWithObjectBind(findAllByUsuarioAndLocalAndEstoque, binds);
        return this.jsonConvert.deserializeArray(result['rows'], EventoDTO);
    }

    async findById(idEvento: Number) : Promise<EventoDTO>{

        if(!idEvento){
            return null;
        }

        let binds: any = {};
        binds.idEvento = idEvento;

        let result = await this.dbService.executeQueryWithObjectBind(findById, binds);
        return this.jsonConvert.deserializeObject(result['rows'][0], EventoDTO);
    }
    
}