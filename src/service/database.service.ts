import { Injectable } from '@nestjs/common';
import * as oracledb from 'oracledb';
import config from '../config/database';
import { LoggerService } from './logger.service';

@Injectable()
export class DataBaseService {
  constructor(private loggerService: LoggerService) { }

  async executeQueryWithObjectBind(statement, binds = {}, opts: any = {}) {
    return new Promise(async (resolve, reject) => {

      opts.outFormat = oracledb.OBJECT;
      opts.autoCommit = true;

      let conn: any;
      try {
        conn = await oracledb.getConnection(config);
        const result = await conn.execute(statement, binds, opts);
        resolve(result);
      } catch (err) {
        reject(err);
      } finally {
        if (conn) {
          try {
            await conn.close();
          } catch (err) {
           this.loggerService.error(err);
          }
        }
      }
    });
  }

  executeQueryWithListBind(statement, binds = [], opts: any = {}) {
    return new Promise(async (resolve, reject) => {

      opts.outFormat = oracledb.OBJECT;
      opts.autoCommit = true;
      let conn: any;
      try {
        conn = await oracledb.getConnection(config);
        const result = await conn.executeMany(statement, binds, opts);
        resolve(result);
      } catch (err) {
        reject(err);
      } finally {
        if (conn) {
          try {
            await conn.close();
          } catch (err) {
           this.loggerService.error(err);
          }
        }
      }
    });
  }
}
