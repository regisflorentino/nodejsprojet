import { Injectable } from '@nestjs/common';
import { LocalEstoqueDTO } from '../dto/local.estoque.dto';
import { findAllByFilial, findAllDestinoByFilialAndOrigem, findById } from '../repository/local.estoque.queries';
import { DataBaseService } from './database.service';
import { LoggerService } from './logger.service';
import _ from 'lodash'
import { JsonConvert } from 'json2typescript';

@Injectable()
export class LocalEstoqueService {

    jsonConvert: JsonConvert;

    constructor(private dbService: DataBaseService, private loggerService: LoggerService) {
        this.jsonConvert = new JsonConvert();
    }

    async findById(id: number): Promise<LocalEstoqueDTO> {
        if(!id){
            return null;
        }
        let binds: any = {};
        binds.id = id;
        let result = await this.dbService.executeQueryWithObjectBind(findById, binds);
        return this.jsonConvert.deserializeObject(result['rows'][0], LocalEstoqueDTO); 
    }

    async findLocalEstoqueByIdFilial(idFilial: number): Promise<LocalEstoqueDTO[]> {
        let binds: any = {};
        binds.idFilial = idFilial;

        let result = await this.dbService.executeQueryWithObjectBind(findAllByFilial, binds);
        return this.jsonConvert.deserializeArray(result['rows'], LocalEstoqueDTO);
    }

    async findLocalEstoqueDestinoByIdFilialAndOrigem(idFilial: number, idOrigem: number): Promise<LocalEstoqueDTO[]> {
        
        let destinos: LocalEstoqueDTO[] = [];

        let binds: any = {};
        binds.idFilial = idFilial;
        binds.idOrigem = idOrigem;

        let result = await this.dbService.executeQueryWithObjectBind(findAllDestinoByFilialAndOrigem, binds);
        let destinosAux: LocalEstoqueDTO[] = this.jsonConvert.deserializeArray(result['rows'], LocalEstoqueDTO);
        let origem: LocalEstoqueDTO = await this.findById(idOrigem);

        if (origem.descriptionLocalEstoque.toUpperCase() === 'CAFETERIA'){
            destinosAux.forEach (item => {
                if (item.descriptionLocalEstoque.toUpperCase() !== 'LOJA'){
                    destinos.push(item);
                }
            });
        } else {
            destinos.push(...destinosAux)
        }

        return destinos;

    }

}