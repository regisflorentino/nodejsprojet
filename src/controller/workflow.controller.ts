import { Controller, Post, Body, Get, Query, Param, Response, HttpStatus } from "@nestjs/common";
import { MipWorkflowBodyDTO } from "../dto/mip.worflow.body.dto";
import { MipService } from "../service/mip.serivce";
import { WorkflowMipPoolService } from "../service/workflow.pool.service";

@Controller('/workflow')
export class WorkflowController {

    constructor(private mipService: MipService, private workflowPoolService: WorkflowMipPoolService){

    }

    @Get('/')
    findAllWorkflowMipByStatus(
        @Query('status') status: string, 
        @Query('itemsPerPage') itemsPerPage: number, 
        @Query('page') page: number){
        return this.mipService.findAllWorkflowMipByStatus(status, itemsPerPage, page);
    }

    @Get('/owners/detalhe/:idDetalhe')
    findOwnersStepWorkflow(@Response() response, @Param('idDetalhe') idDetalhe: number){
        return this.mipService.findOwnerStepWKF(idDetalhe).then(
            data => {response.status(HttpStatus.OK).send(data)}
        );
    }

    @Post('/difinition/merge')
    merge(@Response() response, @Body() bodyWorkflowMip: MipWorkflowBodyDTO){
        return this.workflowPoolService.processWorkflow(bodyWorkflowMip).then(
            id => response.status(HttpStatus.OK).send({id: id, entity: 'OPR_DOC_MIP'})
        );
    }

}