import { Body, Controller, Get, HttpStatus, Param, Post, Query, Response, Logger, Header } from "@nestjs/common";
import { ApiUseTags } from "@nestjs/swagger";
import { MipDTO } from "../dto/mip.dto";
import { LoggerService } from "../service/logger.service";
import { MipService } from "../service/mip.serivce";
import { EstornoMipDTO } from "../dto/estorno.mip.dto";
import { HistoricoMipDTO } from "../dto/historico.mip.dto";

@ApiUseTags('MIP')
@Controller('/')
export class MipController {

    constructor(private mipService: MipService, private logger: LoggerService){}

    @Get('numero-documento')
    findNextNumeroDocumento(@Response() response){
        this.logger.debug('Teste mip');
        this.logger.info('Teste mip');
        this.logger.error('Errrou');
        return this.mipService.findNextNextDocument().then(data => { response.status(HttpStatus.OK).send(data) });
    }

    @Post('save')
    saveMip(@Body() mipObject: MipDTO){
        this.mipService.save(mipObject);
    }

    @Get('mip')
    findAllMip(@Response() response, @Query('itemsPerPage') itemsPerPage: number, @Query('page') page: number){
        return this.mipService.findAllMip(itemsPerPage, page)
                .then(data => { response.status(HttpStatus.OK).send(data)} );
    }

    @Get('mip/:idMip')
    findMipById(@Response() response, @Param('idMip') idMip: number){
        return this.mipService.findMipById(idMip)
                .then(data => { response.status(HttpStatus.OK).send(data)} );
    }

    @Get('mip/:idMip/items')
    findItemsMipByCapaId(@Response() response, @Param('idMip') idMip: number){
        return this.mipService.findItemsDetalheMipByCapaId(idMip).then(
            data => { response.status(HttpStatus.OK).send(data)});
    }

    @Post('mip/estornar/:tipo')
    estornarMip(@Response() response, @Body() estornoDTO: EstornoMipDTO, @Param('tipo') tipo: string){
        return this.mipService.estornarMip(estornoDTO, tipo).then(
            data => { response.status(HttpStatus.OK).send(data) });
    }

    @Post('mip/historico')
    saveHistoricoMip(@Response() response, @Body() historico: HistoricoMipDTO){
        return this.mipService
            .saveHistorico(historico)
            .then(data => {response.status(HttpStatus.OK).send()})
            .catch(err => response.status(HttpStatus.INTERNAL_SERVER_ERROR).send(err));
    }

    @Get('mip/historico/:noDocumento')
    findHistoricoByNumeroDocumento(@Response() response, @Param('noDocumento') noDocumento: number){
        return this.mipService
            .findHistoricoByNumeroDocumento(noDocumento)
            .then( data => response.status(HttpStatus.OK).send(data));
    }

}