import { Controller, Param, Get, Response, HttpStatus } from "@nestjs/common";
import { ApiUseTags } from "@nestjs/swagger";
import { MotivoMovimentacaoService } from "../service/motivo.movimentacao.service";
import { LoggerService } from "../service/logger.service";

@ApiUseTags('MIP')
@Controller('/motivo-movimentacao')
export class MotivoMovimentacaoController {
    
    constructor(private motivoMovimentacaoService: MotivoMovimentacaoService, private logger: LoggerService){ }

    @Get('/evento/:idEvento')
    findAllMotivoMovimentacaoByEvento(@Response() response, @Param('idEvento') idEvento: number){
        return this.motivoMovimentacaoService.findMotivoMovimentacaoByEvento(idEvento)
                .then(data => { response.status(HttpStatus.OK).send(data)} );
    }

}