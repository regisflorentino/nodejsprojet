import { Controller, Get, Param, Response, HttpStatus } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { LocalEstoqueService } from '../service/local.estoque.service';
import { LoggerService } from '../service/logger.service';

@ApiUseTags('MIP')
@Controller('/local-estoque')
export class LocalEstoqueController {

    constructor(private localEstoqueService: LocalEstoqueService, private loggerService: LoggerService) { }

    @Get('/filial/:idFilial')
    findLocalEstoqueByIdFilial(@Response() response, @Param('idFilial') idFilial: number) {
        return this.localEstoqueService.findLocalEstoqueByIdFilial(idFilial)
                .then(data => { response.status(HttpStatus.OK).send(data) } );
    }

    @Get('/origem/filial/:idFilial')
    findOrigemByIdFilial(@Response() response, @Param('idFilial') idFilial: number) {
        return this.localEstoqueService.findLocalEstoqueByIdFilial(idFilial)
                .then(data => { response.status(HttpStatus.OK).send(data) } );
    }

    @Get('/destino/filial/:idFilial/origem/:idLocalOrigem')
    findDestinoByIdFilial(@Response() response, @Param('idFilial') idFilial: number, @Param('idLocalOrigem') idLocalOrigem: number) {
        return this.localEstoqueService.findLocalEstoqueDestinoByIdFilialAndOrigem(idFilial, idLocalOrigem)
                .then(data => { response.status(HttpStatus.OK).send(data) } );
    }

}
