import { Controller, Get, HttpStatus, Param, Response } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { EventoService } from '../service/evento.service';
import { LoggerService } from '../service/logger.service';

@ApiUseTags('MIP')
@Controller('/evento')
export class EventoController {

    constructor(private eventoService: EventoService, private loggerService: LoggerService) { }

    @Get('/user/:idUser/filial/:idFilial/local-estoque/:idLocalEstoque')
    findEventoByUserAndLocalEstoque( @Response() response,
        @Param('idUser')idUser: number,
        @Param('idFilial')idFilial: number,
        @Param('idLocalEstoque')idLocalEstoque: number) {
        
            return this.eventoService
                    .findEventoByUserAndFilialAndLocalEstoque(idUser, idFilial, idLocalEstoque)
                    .then(data => { response.status(HttpStatus.OK).send(data); });
    }
}
