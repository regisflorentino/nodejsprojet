export const findMipNumberDocument = `SELECT OPRNUMDOCMIP_SEQ.NEXTVAL FROM DUAL`;
export const findMipDocId = `SELECT OPRDOCMIP_SEQ.NEXTVAL FROM DUAL`;
export const findHistoricoId = `SELECT SEQ_MIP_HISTORICO.NEXTVAL FROM DUAL`;

export const findAllWorkflowMipByStatus = 
`
SELECT
    RESULT.ID,
    RESULT.DADOS_JSON
FROM (
    SELECT
        ROWNUM rnum,
        A.*
    FROM
    (
        SELECT
          ITENS.ID_WKF_GENERICO_DETALHE AS ID,
          ITENS.DADOS_JSON
        FROM
          ADM_WKF_GENERICO_CAPA      CAPA
          INNER JOIN ADM_WKF_GENERICO_DETALHE   ITENS ON CAPA.ID_WKF_GENERICO_CAPA = ITENS.ID_WKF_GENERICO_CAPA
          INNER JOIN ADM_WORKFLOW ADM_WKF ON ADM_WKF.ID_WORKFLOW = CAPA.ID_WORKFLOW
        WHERE
          CAPA.STATUS = :status
          AND ITENS.STATUS = :status
          AND ADM_WKF.NM_WORKFLOW = 'MIP WORKFLOW'
        ORDER BY
          CAPA.ID_WKF_GENERICO_CAPA DESC
    ) A
    WHERE
        ROWNUM <= (:itemsPerPage * :page)
) RESULT
WHERE
    RESULT.RNUM > ((:itemsPerPage * :page) - :itemsPerPage)
`;

export const findAllMipPaginated = 
`
SELECT
    RESULT.*
FROM (
    SELECT
        ROWNUM rnum,
        A.*
    FROM
    (
        SELECT 
            capa.ID_DOC_MIP                         AS ID_DOC_MIP,
            filial.NM_FILIAL                        AS FILIAL,
            capa.NO_DOCUMENTO                       AS NO_DOCUMENTO,
            capa.TP_DOCUMENTO                       AS TIPO,
            localEstoque.NM_LOCAL_ESTOQUE           AS LOCAL,
            origem.NM_LOCAL_ESTOQUE                 AS ORIGEM,
            destino.NM_LOCAL_ESTOQUE                AS DESTINO,
            evento.DS_EVENTO_ESTOQUE                AS EVENTO,
            capa.STATUS                             AS STATUS,
            capa.NM_USUARIO_SOLICITACAO             AS NM_SOLICITACAO,
            capa.DT_SOLICITACAO                     AS DT_SOLICITACAO,
            movimento.DS_MOTIVO_MOVIMENTACAO	    AS MOVIMENTACAO,
            capa.TP_ORIGEM                          AS TP_ORIGEM
        FROM 
            OPR_DOC_MIP capa
            INNER JOIN CAM_FILIAL filial ON filial.ID_FILIAL = capa.ID_FILIAL
            LEFT JOIN CAM_LOCAL_ESTOQUE localEstoque ON localestoque.ID_LOCAL_ESTOQUE = capa.ID_LOCAL_ESTOQUE
            LEFT JOIN CAM_LOCAL_ESTOQUE origem ON origem.ID_LOCAL_ESTOQUE = capa.ID_LOCAL_ESTOQUE_ORIGEM
            LEFT JOIN CAM_LOCAL_ESTOQUE destino ON destino.ID_LOCAL_ESTOQUE = capa.ID_LOCAL_ESTOQUE_DESTINO
            LEFT JOIN CAM_EVENTO_ESTOQUE evento ON evento.ID_EVENTO_ESTOQUE = capa.ID_EVENTO_ESTOQUE
            LEFT JOIN OPR_MOTIVO_MOVIMENTACAO movimento ON movimento.ID_MOTIVO_MOVIMENTACAO = capa.ID_MOTIVO_MOVIMENTACAO
        ORDER BY
            capa.ID_DOC_MIP DESC
    ) A
    WHERE
        ROWNUM <= (:itemsPerPage * :page)
) RESULT
WHERE
    RESULT.RNUM > ((:itemsPerPage * :page) - :itemsPerPage)
`;

export const findMipById = 
`
SELECT 
    capa.ID_DOC_MIP,
    capa.ID_FILIAL,
    capa.NO_DOCUMENTO,
    capa.TP_DOCUMENTO,
    capa.ID_LOCAL_ESTOQUE,
    capa.ID_LOCAL_ESTOQUE_ORIGEM,
    capa.ID_LOCAL_ESTOQUE_DESTINO,
    capa.ID_EVENTO_ESTOQUE,
    capa.STATUS,
    capa.NM_USUARIO_SOLICITACAO,
    capa.DT_SOLICITACAO,
    capa.ID_MOTIVO_MOVIMENTACAO,
    capa.TP_ORIGEM,
    capa.ID_USUARIO_CADASTRO,
    capa.ID_DETALHE_WKF 
FROM 
    OPR_DOC_MIP capa
WHERE
    capa.ID_DOC_MIP = :idMip
`;

export const findMipItemsByCapaId = 
`
SELECT
    PRODUTO.ID_PRODUTO                                                      AS IDPRODUTO,
    PRODUTO.CD_PRODUTO                                                      AS CODIGOPRODUTO,
    PRODUTO.DS_COMPLETA                                                     AS DESCRICAO,
    PRODUTO.FG_ATIVO                                                        AS PRODUTOATIVO,
    PRODUTO.FG_PESAVEL                                                      AS PESAVEL,
    PRODUTO.ID_PRODUTO || ' - ' || PRODUTO.DS_COMPLETA                      AS PRODUTOLABEL,
    ITEM.ID_DOC_MIP_ITEM                                                    AS IDMIPITEM,
    ITEM.PS_PRODUTO                                                         AS PESO,
    ITEM.QT_PRODUTO                                                         AS QTD,
    ITEM.DS_JUSTIFICATIVA                                                   AS JUSTIFICATIVA,
    ITEM.ST_MOVIMENTACAO                                                    AS ST_MOVIMENTACAO,
    EMBALAGEM.ID_EMBALAGEM                                                  AS IDEMBALAGEM,
    EMBALAGEM.ID_PRODUTO                                                    AS EMBIDPRODUTO,
    EMBALAGEM.NO_PESO_BRUTO                                                 AS PESOBRUTO,
    EMBALAGEM.NO_PESO_LIQUIDO                                               AS PESOLIQUIDO,
    EMBALAGEM.QT_EMBALAGEM                                                  AS QTDEMBALGEM,
    TEMB.NM_SIGLA                                                           AS EMBTIPOSIGLA,
    EMBALAGEM.PESAVEL                                                       AS EMBPESAVEL,
    EMBALAGEM.FG_ATIVO                                                      AS EMBATIVO,
    TEMB.NM_TIPO_EMBALAGEM                                                  AS EMBTIPOQUANEMBA,
    EMBALAGEM.QT_EMBALAGEM || ' - ' || TEMB.NM_SIGLA                        AS EMBALAGEMLABEL,
    (
        SELECT 
            B.NM_SIGLA 
        FROM 
            CAM_EMBALAGEM A 
            INNER JOIN CAM_TIPO_EMBALAGEM B 
                ON B.ID_TIPO_EMBALAGEM = A.ID_TIPO_EMBALAGEM 
            WHERE 
                ID_PRODUTO = PRODUTO.ID_PRODUTO 
                AND (TP_TAMANHO = 'MENOR' OR TP_TAMANHO  = 'UNICA')
                AND ROWNUM = 1
    )                                                                       AS MENORSIGLA,
    ITEM.CUSTO_MEDIO_PRODUTO                                                AS CUSTOMEDIOPRODUTO
FROM
    OPR_DOC_MIP_ITEM ITEM
    INNER JOIN CAM_PRODUTO PRODUTO ON PRODUTO.ID_PRODUTO = ITEM.ID_PRODUTO
    INNER JOIN CAM_EMBALAGEM EMBALAGEM ON EMBALAGEM.ID_EMBALAGEM = ITEM.ID_EMBALAGEM
    INNER JOIN CAM_TIPO_EMBALAGEM TEMB ON TEMB.ID_TIPO_EMBALAGEM = EMBALAGEM.ID_TIPO_EMBALAGEM
WHERE
    ITEM.ID_DOC_MIP = :idCapa
`

export const findOwnersStepWorkflow = 
`
SELECT
    ETAPA.DS_ETAPA AS STEP_WKF,
    NVL(USU_APV.NM_USUARIO,'--') AS OWNER,
    NVL(VV.DT_ALTERACAO, null) AS DATE_ACTION
FROM
    ADM_ETAPA_WKF ETAPA
    INNER JOIN ADM_WORKFLOW WKF ON ETAPA.ID_WORKFLOW = WKF.ID_WORKFLOW AND WKF.NM_WORKFLOW = 'MIP WORKFLOW'
    LEFT JOIN (
        SELECT
            ID_WKF_GENERICO_DETALHE,
            ID_ETAPA,
            STATUS,
            ID_USUARIO_ALTERACAO,
            DT_ALTERACAO
        FROM
            ADM_WKF_GENERICO_DETALHE
        WHERE
            ID_WKF_GENERICO_DETALHE >= 
            (
                SELECT 
                        MAX(ID_WKF_GENERICO_DETALHE) AS ID_PRIMEIRA_ETAPA
                    FROM 
                        ADM_WKF_GENERICO_DETALHE DETALHE_WKF
                        INNER JOIN ADM_ETAPA_WKF ETAPA ON ETAPA.ID_ETAPA = DETALHE_WKF.ID_ETAPA AND NO_SEQUENCIA = 1
                    WHERE
                        DETALHE_WKF.ID_WKF_GENERICO_CAPA = :idCapa AND DETALHE_WKF.STATUS = 'APROVADO'
            )
            AND ID_WKF_GENERICO_CAPA = :idCapa AND STATUS = 'APROVADO'
    ) VV ON VV.ID_ETAPA = ETAPA.ID_ETAPA AND (SELECT COUNT(0) FROM ADM_WKF_GENERICO_DETALHE WHERE ID_WKF_GENERICO_CAPA = :idCapa AND STATUS = 'REPROVADO' AND ID_WKF_GENERICO_DETALHE > VV.ID_WKF_GENERICO_DETALHE) < 1
    LEFT JOIN ADM_USUARIO USU_APV ON USU_APV.ID_USUARIO = VV.ID_USUARIO_ALTERACAO
`;

export const findCapaIdFromDetalheId =
`
SELECT
    CAPA_WKF.ID_WKF_GENERICO_CAPA
FROM
    ADM_WKF_GENERICO_CAPA CAPA_WKF
    INNER JOIN ADM_WKF_GENERICO_DETALHE ITEM_WKF ON CAPA_WKF.ID_WKF_GENERICO_CAPA = ITEM_WKF.ID_WKF_GENERICO_CAPA
WHERE
    ITEM_WKF.ID_WKF_GENERICO_DETALHE = :idDetalhe
`;

export const insertMipCapa =
`
INSERT INTO OPR_DOC_MIP (
    ID_DOC_MIP,
    ID_FILIAL,
    NO_DOCUMENTO,
    TP_DOCUMENTO,
    ID_EVENTO_ESTOQUE,
    ID_LOCAL_ESTOQUE,
    ID_LOCAL_ESTOQUE_ORIGEM,
    ID_LOCAL_ESTOQUE_DESTINO,
    STATUS,
    ID_USUARIO_CADASTRO,
    ID_USUARIO_ALTERACAO,
    DT_CADASTRO,
    DT_ALTERACAO,
    ID_DETALHE_WKF,
    NM_USUARIO_SOLICITACAO,
    DT_SOLICITACAO,
    ID_MOTIVO_MOVIMENTACAO,
    TP_ORIGEM
) VALUES (
    :idMip,
    :idFilial,
    :numeroDocumento,
    :tipo,
    :evento,
    :local,
    :origem,
    :destino,
    UPPER(:status),
    :idUsuarioCadastro,
    :idUsuarioAlteracao,        
    sysdate,
    :dtAlteracao,
    :idDetalheWkf,
    :nmSolicitante,
    :dataSolicitacao,
    :motivoMovimentacao,
    :tpOrigem
)
`;

export const insertMipDetalhe = 
`
INSERT INTO OPR_DOC_MIP_ITEM (
    ID_DOC_MIP_ITEM,
    ID_DOC_MIP,
    ID_PRODUTO,
    ID_EMBALAGEM,
    QT_PRODUTO,
    PS_PRODUTO,
    DS_JUSTIFICATIVA,
    ST_MOVIMENTACAO,
    CUSTO_MEDIO_PRODUTO
) VALUES (
    OPRDOCMIPITEM_SEQ.nextval,
    :idMip,
    :idProduto,
    :idEmbalagem,
    :qtdProduto,
    :pesoProduto,
    :justificativa,
    UPPER(:statusMovimentacao),
    :custoMedioProduto
)
`;

export const insertEstornoMipHorus = 
`
    INSERT INTO OPR_DOC_MIP_ITEM_ESTORNO (
        ID,
        PESO_ANTERIOR,
        PESO_NOVO_VALOR,
        QTD_ANTERIOR,
        QTD_NOVO_VALOR,
        EMBALAGEM_ANTERIOR,
        EMBALAGEM_NOVO_VALOR,
        ID_PRODUTO,
        ID_FILIAL,
        ID_DOC_MIP,
        ID_DOC_MIP_ITEM,
        DT_FATO
    ) VALUES (
        seq_mip_item_estorno.NEXTVAL,
        :pesoAnterior,
        :pesoNovoValor,
        :quantidadeAnterior,
        :quantidadeNovoValor,
        :embalagemAnterior,
        :embalagemNovoValor,
        :idProduto,
        :idFilial,
        :idDocMip,
        :idDocMipItem,
        SYSDATE
    )
`;

export const updateStatusMipCapa =
`
UPDATE 
    OPR_DOC_MIP
SET
    STATUS = :status
WHERE
    ID_DOC_MIP = :idDocMip
`

export const updateMipDetalhe = 
`
UPDATE 
    OPR_DOC_MIP_ITEM
SET
    ID_EMBALAGEM = :idEmbalagem,
    QT_PRODUTO = :qtdProduto,
    PS_PRODUTO = :pesoProduto,
    DS_JUSTIFICATIVA = :justificativa,
    ST_MOVIMENTACAO = UPPER(:statusMovimentacao),
    CUSTO_MEDIO_PRODUTO = :custoMedioProduto
WHERE
    ID_DOC_MIP_ITEM = :idDocMipItem
`;

export const deleteMipItem = `DELETE FROM OPR_DOC_MIP_ITEM WHERE ID_DOC_MIP_ITEM = :idDocMipItem`;

export const insertHistoricoMip = 
`
INSERT INTO OPR_DOC_MIP_HISTORICO (
    ID,
    NO_DOCUMENTO,
    TYPE,
    ID_USER,
    DESCRICAO,
    DATA_FATO
) VALUES (
    :id,
    :noDocumento,
    :type,
    :idUser,
    :descricao,
    sysdate
)
`;

export const insertHistoricoMipItem = 
`
INSERT INTO OPR_DOC_MIP_HISTORICO_ITEM (
    ID,
    ID_MIP_HISTORICO,
    ID_EMBALAGEM,
    EMBALAGEM_MOD,
    ID_EMBALAGEM_NOVO,
    QUANTIDADE,
    QUANTIDADE_MOD,
    QUANTIDADE_NOVO,
    PESO,
    PESO_MOD,
    PESO_NOVO,
    CUSTO_MEDIO,
    ID_PRODUTO,
    DESCRICAO,
    ACTION,
    DATA_FATO
) VALUES (
    seq_mip_historico_item.NEXTVAL,
    :idMipHistorico,
    :idEmbalagem,
    :embalagemMod,
    :idEmbalagemNovo,
    :quantidade,
    :quantidadeMod,
    :quantidadeNovo,
    :peso,
    :pesoMod,
    :pesoNovo,
    :custoMedio,
    :idProduto,
    :descricao,
    :acao,
    sysdate
)
`

export const findHistoricoMipByNumeroDocumento = 
`
SELECT 
    ID,
    NO_DOCUMENTO,
    TYPE,
    ID_USER,
    DESCRICAO,
    DATA_FATO
FROM 
    OPR_DOC_MIP_HISTORICO
WHERE
    NO_DOCUMENTO = :noDocumento
ORDER BY
    DATA_FATO DESC
`

export const findItemsHistoricoByhistoricoId = 
`
SELECT
    ID,
    ID_MIP_HISTORICO,
    ID_EMBALAGEM,
    EMBALAGEM_MOD,
    ID_EMBALAGEM_NOVO,
    QUANTIDADE,
    QUANTIDADE_MOD,
    QUANTIDADE_NOVO,
    PESO,
    PESO_MOD,
    PESO_NOVO,
    CUSTO_MEDIO,
    ID_PRODUTO,
    DESCRICAO,
    DATA_FATO,
    ACTION
FROM
    OPR_DOC_MIP_HISTORICO_ITEM 
WHERE
    ID_MIP_HISTORICO = :idHistorico
`