export const findById = 
`SELECT 
    CLE.ID_LOCAL_ESTOQUE 	                                        AS 	ID_LOCAL_ESTOQUE,
    CLE.NM_LOCAL_ESTOQUE 	                                        AS 	NM_LOCAL_ESTOQUE,
    CLE.CD_LOCAL_ESTOQUE || ' - ' || CLE.NM_LOCAL_ESTOQUE           AS ID_AND_NAME
FROM 
	CAM_LOCAL_ESTOQUE CLE
WHERE
    CLE.ID_LOCAL_ESTOQUE = :id
ORDER BY
    CLE.CD_LOCAL_ESTOQUE`
;

export const findAllByFilial = 
`SELECT 
    CLE.ID_LOCAL_ESTOQUE 	                                        AS 	ID_LOCAL_ESTOQUE,
    CLE.NM_LOCAL_ESTOQUE 	                                        AS 	NM_LOCAL_ESTOQUE,
    CLE.CD_LOCAL_ESTOQUE || ' - ' || CLE.NM_LOCAL_ESTOQUE           AS ID_AND_NAME
FROM 
	CAM_LOCAL_ESTOQUE CLE
    INNER JOIN CAM_LOCAL_ESTOQUE_FILIAL CLEF ON CLE.ID_LOCAL_ESTOQUE = CLEF.ID_LOCAL_ESTOQUE
WHERE
    CLEF.ID_FILIAL = :idFilial
ORDER BY
    CLE.CD_LOCAL_ESTOQUE`
;

export const findAllDestinoByFilialAndOrigem = 
`SELECT 
    CLE.ID_LOCAL_ESTOQUE 	                                        AS 	ID_LOCAL_ESTOQUE,
    CLE.NM_LOCAL_ESTOQUE 	                                        AS 	NM_LOCAL_ESTOQUE,
    CLE.CD_LOCAL_ESTOQUE || ' - ' || CLE.NM_LOCAL_ESTOQUE           AS ID_AND_NAME
FROM 
	CAM_LOCAL_ESTOQUE CLE
    INNER JOIN CAM_LOCAL_ESTOQUE_FILIAL CLEF ON CLE.ID_LOCAL_ESTOQUE = CLEF.ID_LOCAL_ESTOQUE
WHERE
    CLEF.ID_FILIAL = :idFilial
    AND CLE.ID_LOCAL_ESTOQUE NOT IN (:idOrigem)
ORDER BY
    CLE.CD_LOCAL_ESTOQUE`
;
